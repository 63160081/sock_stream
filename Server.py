import socket

def main():
    host = '127.0.0.1'
    port = 12345

    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_socket.bind((host, port))
    server_socket.listen(1)
    print("Server listening on {}:{}".format(host, port))

    client_socket, client_address = server_socket.accept()
    print("Connected by:", client_address)

    with open('a.txt', 'rb') as file:
        while True:
            data = file.read(1024)
            if not data:
                break
            client_socket.send(data)

    client_socket.close()
    server_socket.close()
    print("Connection closed")

if __name__ == "__main__":
    main()
